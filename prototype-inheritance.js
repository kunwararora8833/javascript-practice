// inheritance using 'prototype'     this method can inheritance properties from an object or another constructor
let user = { 
    job: 'software Developer',
    city: 'Delhi'
};
let Person = function(name, experience){
    this.name = name;
    this.experience = experience
};

// prototype will inheritance all the properties of 'user' object to 'Person' contructor  
Person.prototype = user;

// prototype also add some functions to that particular constructor
Person.prototype.getInfo = function(){console.log(`${this.name} from ${this.city}, Work as a ${this.job}`)};

let shawn = new Person('Shawn', 3);
shawn.getInfo();
console.log(shawn);

console.log('')




// Inheritance in ES6      this method will only inheritance properties from another 'class'
class User2 {
    constructor(){
        // you can set these as default or use constructor to input these values(As in object.js file)
        this.job = 'software Developer'; 
        this.city = 'Delhi';
    };
};
class Person2 extends User2{
    constructor(name, experience){
        super();       // super keyword inheritance all properties from extends class, user 'super' before using this keywords
        this.name = name;
        this.experience = experience;
    };
    getInfo(){console.log(`${this.name} from ${this.city}, Work as a ${this.job}`)};
};

let john = new Person2('John', 5);
console.log(john)
john.getInfo();
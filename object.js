// Old way
var Students = function(firstName, section, rollNumber){
    this.firstName = firstName;
    this.section = section;
    this.rollNumber = rollNumber;
} 

var john = new Students('John', 'A', 35);
var sam = new Students('Sam', 'B', 40);
console.log(john);
console.log(sam);

Students.prototype.getFirstName = function(){
    console.log(`Student name is ${this.firstName}`);
};
john.getFirstName();



// New way(ES6)
class Students2{
    constructor(firstName, section, rollNumber){
        this.firstName = firstName;
        this.section = section;
        this.rollNumber = rollNumber;
    };
    getFirstName(){console.log(`Student name is ${this.firstName}`)};
};
var matt = new Students2('Matt', 'A', 45);
console.log(matt);
matt.getFirstName();

var mike = new Students2('Mike', 'C');
console.log(mike);


// Making subclasses with classes

class Athlete extends Students2{
    constructor(firstName, section, rollNumber, games, medals){
        super(firstName, section, rollNumber);
        this.games = games;
        this.medals = medals;
    }
} 
var jack = new Athlete('Jack', 'A', 20, 'Football', 'Gold');
console.log(jack);

var mathew = new Athlete('Mathew', 'B', 25, ['Football', 'Cricket'], 'Silver');
console.log(mathew);
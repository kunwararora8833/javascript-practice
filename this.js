// this in Objects
let person = {
    name:'Matt',
    printName: function(){
        console.log(this.name);   //this inside the function nested to object is point to object
        console.log(this === person);   // returns true

        //* this code will only run in browser console
        // console.log(this === window);      
    }
};
person.printName();



// this in function
var name2 = 'John'
function sayName(){
    var name = 'John';
    return function displaName(){
        // this inside the function always point to global context i.e window
        console.log(this.name);   // will return undefined 
        console.log(name);  // 'John'


        console.log(this.name2);  // will return 'undefined' in node compiler, but will return 'John' in browser as this in browser means window 
        console.log(name2);  // 'John'
        
        // console.log(this === window);  // return true
    };
};
sayName()();  



// this in Object class
class Obj{
    constructor(name, age){
        // this in object constructor will point to newly object created by Obj constructor
        this.name = name;
        this.age = age;
    };
};
const object = new Obj('Matt', 25);


// watch this video --> https://www.youtube.com/watch?v=gvicrj31JOM

// this --> object that is executing the current function

// if function is part of an object (lets say obj)
// "this" keyword in that function points to the object (obj)

// if normal function
// "this" keyword in that function points to the global object (window in browser, global in node)

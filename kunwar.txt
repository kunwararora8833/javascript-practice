https://www.geeksforgeeks.org/how-to-become-a-javascript-developer/

https://www.pluralsight.com/courses/javascript-promises-async-programming
https://app.pluralsight.com/player?course=javascript-promises-async-programming&author=nate-taylor&name=ebc368bc-9e02-4e8f-b26e-f422c7b5cac9&clip=4&mode=live

https://www.pluralsight.com/courses/angularjs-get-started
https://www.pluralsight.com/courses/building-components-angular-1-5

PART - 1
	closure
	module pattern / revealing module pattern
	hoisting
	singleton
	"this" keyword
	prototype
	context
	Object.create
	prototype inheritance

PART - 2
	go over geeksforgeeks link and see whats left in part1
	
PART - 3
	go over the topics I mentioned on the first day and see whats left

For every topic create notes and do implementation
Every day should start with previous day's notes revision and end with committing code


GIT - https://www.youtube.com/watch?v=8JJ101D3knE&list=PLTjRvDozrdlxCs_3gaqd120LcGxmfe8rG&index=6
SCOPE - https://www.youtube.com/watch?v=iJKkZA215tQ&list=PLTjRvDozrdlxEIuOBZkMAK5uiqp8rHUax&index=13
THIS - https://www.youtube.com/watch?v=gvicrj31JOM&list=PLTjRvDozrdlxEIuOBZkMAK5uiqp8rHUax&index=16
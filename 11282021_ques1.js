/*var add100 = {
    x: 100,
    answer: function(y) {
        console.log(y + this.x);
    },
};
add100.x = 200;
add100.answer(50);*/

//------------------------------------------------

var add100 = (function() {
    var x = 100;
    var z = 90;
    var answer = function (y) {
        console.log(y + x);
    };

    return {
        //x: 200,
        answer: answer,
    };
})();

//var myObj = add100();

add100.x = 300;
add100.answer(100);


// I have implemented 2 ways of doing the same thing i.e adding 100 to a variable and printing the result.
// I am a new developer in the company and am confused which way to commit
// You being an expert I come to you for assistance
// Which way out of the two would you recommend and why?

// function retains the information about the environment in which it is created


var y = 100;

function abc() {
    console.log(y);
}
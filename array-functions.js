//* forEach in array
let arr = [2, 3, 4, 5, 6, 7];
let doubleArr = []; 
arr.forEach(num => doubleArr.push(num *2))    // will loop through all element in array and perform particular actions
console.log(doubleArr)




//* map in array 

let array = [2, 3, 4, 5, 6, 7];
let doubleArray = array.map(num => num*2);  // will loop through all element in array and perform particular actions but will return/add those result in array
console.log(doubleArr);

let colors = ['blue', 'green', 'red', 'black'];
console.log('before map ', colors);
let newColors = colors.map(color =>  color === 'red' ? 'orange' : color)
console.log('after map ' , newColors)




//* fill in array
let arr2 = [2, 3, 4, 5, 6, 7];

// fill("value", starting Index, ending Index)
let fillArr2 = arr2.fill('a', 2, 5);   // will change/fill the array elements that includes starting Index and excludes ending index
console.log(fillArr2)




//* filter in array

let arr3 = [10, 12, 14, 16, 55, 60];

// will run this particular code/condition and check for each element in array  
let filterArr3 = arr3.filter( num => num < 50)    // it will filter or delete the value that returns false for condition
console.log(filterArr3)

let names = ["Harry", "Sam", "Matt", "John"];
let filterName = names.filter((name, index) =>  {return name != "Sam" && index != 2});
console.log(filterName);
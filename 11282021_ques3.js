/*var x = (function() {
    console.log(this);
})();*/

//-------------------------

//console.log(this);

//---------------------------

/*var y = {
    answer: function() {
        console.log(this);
    },
}
y.answer();*/

//------------------------------

function anyFunc() {
    console.log(this);
}

anyFunc();

//------------------------------

/*function MyFunc() {
    console.log(this);
}

const x = new MyFunc();*/

//------------------------------

/*var y = {
    addNum: 10,
    answer: function() {
        var numbers = [1, 2, 3];

        numbers.forEach(function(number) {
            console.log(number + this.addNum)
        });
    },
}
y.answer();*/

// ---------------------------------------

/*var y = {
    addNum: 10,
    answer: function() {
        var numbers = [1, 2, 3];

        numbers.forEach(function(number) {
            console.log(number + this.addNum)
        }, this);
    },
}
y.answer();*/

// ---------------------------------------

/*var y = {
    addNum: 10,
    answer: function() {
        var numbers = [1, 2, 3];
        numbers.forEach(number => console.log(number + this.addNum));
    },
}
y.answer();*/
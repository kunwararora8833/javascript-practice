// Without using Singleton
class Students2{
    constructor(){
        this.student = {
            name: 'Matt',
            rollNumber: Math.floor(Math.random() * 99 + 1)
        };
        //Object.freeze(this.student);
    };
};

// as this will make new instance of 'students2' class
const c = new Students2();
const d = new Students2();
console.log(c);
console.log(d);
console.log(c === d)
//console.log('');



// With using Singleton
class Students{
    constructor(){
        if(Students.instance instanceof Students){
            return Students.instance;
        }
        this.student = {
            name: 'Kunwar',
            rollNumber: Math.floor(Math.random() * 99 + 1)
        };
        //Object.freeze(this.student);
        //Object.freeze(this);
        Students.instance = this;
    }
};
// As this will make only one new instance of 'students' class,
// And all another new instances will be same as the first one
const a = new Students();
const b = new Students();

console.log(a);
console.log(b);
console.log(a === b);


// try and use very simple examples to explain concepts.
// the one you have stated above is fine but very complicated.
// if someone asks you to implement a singleton in js you won't go and implement the above thing
// instead you would simply do this --> var x = {};
// x is a singleton. you can't have 2 separate instances of x running in the same scope.

// and why have you used Object.freeze()?
// i can still demonstrate singleton after commenting out Object.freeze
// so again try and use very small less complicated code snippets
// that would impress me more
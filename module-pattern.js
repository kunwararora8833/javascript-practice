// Module pattern
var displayPerson = (function(){
    // private varriable
    let person = {
        name: 'Matt',
        rollNumber: 45 
    };

    // private function
    function setMarks(subjectName, marks){
        console.log(`${person.name} scored ${marks} in ${subjectName}`)
    };

    // return only those variable which you want to expose to public
    return{
        // this public function will call the private function inside it
        setMarks: function(subjName, mk){
            setMarks(subjName, mk);
        }
    };
})();
console.log(displayPerson.person);   // returns undefined
displayPerson.setMarks('English', 50);  





// Module pattern using singleton
var displayPerson = (function(){
    var instance;
    function init(){
        var person = 'John';
        var marks = Math.floor(Math.random() * 50 + 20)
        function setMarks(){
            console.log(`${person} scored ${marks}`);
        };

        // returns only variable or function expose to public
        return{
            displayMarks: function(){
                setMarks();
            },
            marks: marks
        };
    };

    return{
        // check if there is already one instance or not
        getInstance: function(){
            if(!instance){
                instance = init();   // if there is no instance then make function 'init()' as only instance
            }
            return instance;
        }
    };
    
})();
var person1 = displayPerson.getInstance();
var person2 = displayPerson.getInstance();
person1.displayMarks();
person2.displayMarks();
console.log(person1.marks === person2.marks);  // true

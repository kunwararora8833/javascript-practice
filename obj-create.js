// Object.create()
// to make new object with prototype set to a certain object

let person = {
    city: 'Ludhiana',
    printDetails: function(){
        console.log(`My Name is ${this.name}, i am a ${this.job}. I live in ${this.city}`)
    }
}
let matt = Object.create(person);
matt.name = 'Matt';
matt.job = 'teacher';
// console.log(matt);
console.log(matt.hasOwnProperty('city'));
console.log(person.isPrototypeOf(matt) ? 'True, matt is prototype of person object' : "matt is not a prototype of person object");
matt.printDetails();

console.log('');

let john = Object.create(person);
john.name = 'John';
john.job = 'software Engineer';
john.city = 'Delhi';
console.log(john.hasOwnProperty('city'));
john.printDetails();